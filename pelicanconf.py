#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Pietro Abate'
AUTHOR_URL = 'pages/about.html'
SITENAME = u'Pietro Abate'
SITEURL = ''

CC_LICENSE = "CC-BY-SA"
CC_ATTR_MARKUP = True
CUSTOM_LICENSE = "Creative Commons Attribution-Share Alike 2.0 France License."
ABOUT_ME = 'Free Software enthusiast, Senior Researcher at <a href="https://www.nomadic-labs.com">Nomadic Labs</a>'

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'en'
I18N_TEMPLATES_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DISPLAY_CATEGORIES_ON_MENU = False
DISPLAY_CATEGORIES_ON_SIDEBAR = False
DISPLAY_RECENT_POSTS_ON_SIDEBAR = False

MENUITEMS = (
#    ('Publications', 'publications.html'),
    ('Nomadic-Labs', 'http://www.nomadic-labs.com/'),
    ('Tezos', 'http://www.tezos.com/'),
    ('Irill', 'http://www.irill.org/'),
)

# Blogroll
LINKS = ()

# Social widget
SOCIAL = (
    ('GitLab', 'http://gitlab.com/abate'),
    ('GitHub', 'http://github.com/abate'),
    ('StackOverflow', 'http://stackoverflow.com/users/741720/pietro-abate',
     'stack-overflow'),
)

DEFAULT_PAGINATION = 5
SUMMARY_MAX_LENGTH = None
TYPOGRIFY = True
THEME = 'themes/pelican-bootstrap3'
BOOTSTRAP_THEME = 'superhero'
THEME_STATIC_DIR = 'themes/pelican-bootstrap3/static/'

DIRECT_TEMPLATES = (('index', 'tags', 'archives', 'search'
                     # ,'publications'
                     ))
STATIC_PATHS = ['theme/images', 'images',
                'images', 'pdfs', 'files', 'theme/css']

PLUGIN_PATHS = ['plugins']

PUBLICATIONS_SRC = 'content/biblio.bib'
PLUGINS = [
    'tag_cloud',
    'sitemap',
    'tipue_search',
    'extract_toc',
    'i18n_subsites',
    'pelican-cite']

#MD_EXTENSIONS = ['codehilite(css_class=highlight)', 'extra', 'headerid', 'toc']
MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.codehilite': {'css_class': 'highlight'},
        'markdown.extensions.extra': {},
        'markdown.extensions.toc': {},
    },
    'output_format': 'html5',
}

SITEMAP = {
    'format': 'xml',
    'priorities': {
        'articles': 0.5,
        'indexes': 0.5,
        'pages': 0.5
    },
    'changefreqs': {
        'articles': 'monthly',
        'indexes': 'daily',
        'pages': 'monthly'
    }
}

JINJA_ENVIRONMENT = {
    'extensions': ['jinja2.ext.i18n'],
}

TAG_CLOUD_MAX_ITEMS = 20
SHOW_ARTICLE_CATEGORY = False
DISPLAY_ARTICLE_INFO_ON_INDEX = True

USE_PAGER = True

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
