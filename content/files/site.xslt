<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:output method="xml" indent="yes" />

    <xsl:template match="*|/">
	<xsl:apply-templates />
    </xsl:template>
    
    <xsl:template name="normalize">
	<xsl:param name="input"/>
	<xsl:param name="mode">normal</xsl:param>
	<xsl:variable name="one">
	    <xsl:value-of select="translate($input, '{{','')"/>
	</xsl:variable>
	<xsl:variable name="two">
	    <xsl:value-of select="translate($one, '}}','')"/>
	</xsl:variable>
	<xsl:variable name="three">
	    <xsl:value-of select="translate($two, '%%','')"/>
	</xsl:variable>
	<xsl:choose>
	    <xsl:when test="$mode = 'escape'">
		<xsl:value-of disable-output-escaping="yes" select="$three"/>
	    </xsl:when>
	    <xsl:otherwise>
		<xsl:value-of select="$three"/>
	    </xsl:otherwise>
	</xsl:choose>
    </xsl:template> 

    <xsl:template match="text()|@*">
	<xsl:call-template name="normalize">
	    <xsl:with-param name="input">
		<xsl:value-of select="."/>
	    </xsl:with-param>
	</xsl:call-template>
    </xsl:template> 

    <xsl:template match="/page">
	<xsl:param name="level">1</xsl:param>  
		<h1><xsl:value-of select="title/text()"/></h1>
		<xsl:apply-templates select="page">
		    <xsl:with-param name="level">
			<xsl:value-of select="$level + 1"/>
		    </xsl:with-param>
		</xsl:apply-templates>
		<xsl:apply-templates select="box">
		    <xsl:with-param name="level">
			<xsl:value-of select="$level + 1"/>
		    </xsl:with-param>
		</xsl:apply-templates>
		<xsl:apply-templates select="include"/>
    </xsl:template>

    <xsl:template match="include">
	<xsl:param name="level">1</xsl:param>  
	<xsl:apply-templates select="document(@file)/page">
	    <xsl:with-param name="level">
		<xsl:value-of select="$level"/>
	    </xsl:with-param>
	</xsl:apply-templates>
    </xsl:template>

    <xsl:template name="page" match="page">
	<xsl:param name="level"/>  
	<xsl:element name="h{$level}">
	    <xsl:value-of select="title/text()"/>
	</xsl:element>
	<p>
	    <xsl:apply-templates>
		<xsl:with-param name="level">
		    <xsl:value-of select="$level + 1"/>
		</xsl:with-param>
	    </xsl:apply-templates>
	</p>
    </xsl:template>

    <xsl:template match="box">
	<xsl:param name="level"/>  
	<xsl:element name="h{$level}">
	    <xsl:value-of select="@title"/>
	</xsl:element>
	<p>
	    <xsl:apply-templates>
		<xsl:with-param name="level">
		    <xsl:value-of select="$level + 1"/>  
		</xsl:with-param>
	    </xsl:apply-templates>
	</p>
    </xsl:template>

    <xsl:template match="sample">
	<code lang="ocaml">
	    <xsl:call-template name="normalize">
		<xsl:with-param name="input">
		    <xsl:value-of select="."/>
		</xsl:with-param>
		<xsl:with-param name="mode">escape</xsl:with-param>
	    </xsl:call-template>
	</code>
    </xsl:template>

    <xsl:template match="xmlsample">
	<code lang="xml">
	    <xsl:call-template name="normalize">
		<xsl:with-param name="input">
		    <xsl:value-of select="."/>
		</xsl:with-param>
		<xsl:with-param name="mode">escape</xsl:with-param>
	    </xsl:call-template>
	</code>
    </xsl:template>

    <xsl:template match="sessionsample">
	<code>
	    <xsl:call-template name="normalize">
		<xsl:with-param name="input">
		    <xsl:value-of select="."/>
		</xsl:with-param>
		<xsl:with-param name="mode">escape</xsl:with-param>
	    </xsl:call-template>
	</code>
    </xsl:template>

    <xsl:template match="twocolumns">
	<table>
	    <tr>
		<td> <xsl:value-of select="left"/></td>
		<td> <xsl:value-of select="right"/></td>
	    </tr>
	</table>
    </xsl:template>

    <xsl:template match="p">
	<p><xsl:apply-templates /></p>
    </xsl:template>

    <xsl:template match="code">
	<b><xsl:apply-templates /></b>
    </xsl:template>

    <xsl:template match="ol">
	<ol><xsl:apply-templates /></ol>
    </xsl:template>

    <xsl:template match="ul">
	<ul><xsl:apply-templates /></ul>
    </xsl:template>

    <xsl:template match="li">
	<li><xsl:apply-templates /></li>
    </xsl:template>

    <xsl:template match="table">
	<xsl:copy-of select="." />
    </xsl:template>

    <xsl:template match="a">
	<xsl:copy-of select="." />
    </xsl:template>

    <xsl:template match="local">
	<a> <xsl:attribute name="href">#<xsl:value-of select="@href"/></xsl:attribute> Link </a>
    </xsl:template>

</xsl:stylesheet>
