Title: About

About Me
========

I received my Ph.D. in theoretical Computer Science from the [Australian
National University][1] (ANU) in 2006. During my postgraduate studies I
developed the [Tableau WorkBench][2], a generic theorem prover that is both
easy to use and flexible accepting specification in different formats.  Since
then I've been involved with many academic research projects at The University
Paris Direrot (Paris 7), [laboratoire PPS (now Iriff)][3]. In particular the
[Mancoosi][4] project and the [CDuce][5] project.

I've been part of the [INRIA team Gallium][6] and of the [Center for Research
and Innovation on Free Software (IRILL)][7]. I'm a strong supporter of the
[Debian][8] project (despite I never officially joined it ).

From 2017 I've joined [Nomadic Labs][9] a French company working on the
development of [Tezos][10] technology.

My current academic interests are software engineering, blockchain technology
and functional programming, modal logic. 

Detailed CV on request.

[1]: https://www.anu.edu.au
[2]: http://twb.rsise.anu.edu.au/
[3]: https://www.irif.fr/
[4]: http://www.mancoosi.org/
[5]: http://www.cduce.org/
[6]: http://gallium.inria.fr/
[7]: https://www.irill.org
[8]: https://www.debian.org
[9]: https://www.nomadic-labs.com
[10]: https://tezos.com
