Title: RedMine experiments
Slug: redmine-experiments
Tags: hacking
Date: 2008-02-29 15:45:23 CET+0000

[RedMine](http://redmine.org/) is a hosting solution for Open projects
conceptually similar to [Trac](http://trac.org). At first sight, the
main difference with trac is that redmine is multi-project, while trac
has a narrower vision where each trac instance is a project. To be
fair there are extensions in trac to administer multiple projects at
once, built this is not shipped with the system.

It's written in ruby. This is not a problem per-se, but since I never
worked with Rails, it took a bit more then necessary to get it going.
In the end, I configure redmine to run as a cgi as I didn't manage to
get the fast-cgi interface going. The problem is mostly related with
apache.
It's fairly slow in this way, but this is by design.

I've used Rails 2.0 shipped with debian unstable on a stable machine.
The installation was painless as rails doesn't have conflicting
dependencies.

I've to play with it a bit more, but it seems a good system , somehow
better then trac if you want to have a forge-style site without
installing gforge itself.


