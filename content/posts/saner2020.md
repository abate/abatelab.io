Title: Dependency Solving: Looking Back, Going Forward
Date: 2020/02/17

## Abstract
  Dependency solving is a hard (NP-complete) problem in all non-trivial
  component models due to either mutually incompatible versions of the same
  packages or explicitly declared package conflicts. As such, software upgrade
  planning needs to rely on highly specialized dependency solvers, lest falling
  into pitfalls such as incompleteness---a combination of package versions that
  satisfy dependency constraints does exist, but the package manager is unable
  to find it.

  In this paper we look back at proposals from dependency solving research
  dating back a few years. Specifically, we review the idea of treating
  dependency solving as a separate concern in package manager implementations,
  relying on generic dependency solvers based on tried and tested techniques
  such as SAT solving, PBO, MILP, etc.

  By conducting a census of dependency solving capabilities in state-of-the-art
  package managers we conclude that some proposals are starting to take off
  (e.g., SAT-based dependency solving) while---with few exceptions---others
  have not (e.g., outsourcing dependency solving to reusable components). We
  reflect on why that has been the case and look at novel challenges for
  dependency solving that have emerged since.

*Authors:* Pietro Abate, Roberto Di Cosmo, Georgios Gousios and Stefano Zacchiroli

We are going to be present this work during the next edition of the [*IEEE
International Conference on Software Analysis, Evolution and Reengineering
SANER ERA 2020*][1] .

The detailed program [here][2]

[1]: https://saner2020.csd.uwo.ca/
[2]: https://saner2020.csd.uwo.ca/detailedProgram
