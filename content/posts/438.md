Title: lesson learned making a cargo kilt
Slug: lesson-learned-making-cargo-kilt
Tags: debian, hacking, kilt, sewing
Date: 2012-10-25 10:31:00 CET+0000

This entry is not really about computers, technology,  or other
work-related topics, but more about a hard-hack that I wanted to try
for a while. How to make a **kilt** !!!

After a bit of duck-ducking, I decided to follow this excellent
[tutorial](http://www.instructables.com/id/How-to-make-a-Cargo-Kilt/?ALLSTEPS).
At first sight the entire process seems  a bit long, but you will
realize that after the first read, that everything boils down to 3
steps: measure, fold and pin, sew.

For the measure part, I've the impression that the formula that is
given in the instructable (waist/3*8+1)  is a bit short for my comfort
and taste.  This is the size for the internal apron, the folded part
that goes all around your left hip, back, and right hip, and the front
apron. My suggestion would be to make the inner apron a bit longer
then the front apron. This way the kilt will feel in my opinion more
comfortable and it will envelop you body completely.

For the fold and pin part, you just need a bit of patience and a
ruler. Put the pins parallel to the folding as in the instructable and
not perpendicular. This will help you later when sewing everything.

The sewing ... If you know how to use a sewing machine, this is going
to be a piece of cake. Otherwise, well, I spent more time
troubleshooting the machine then sewing the kilt. I broke a few
needles in the process and learned how to thread the machine with my
eyes blindfolded. Not to mention that you have to learn how to
disassemble this machines in a thousand parts to understand how the
thread got stuck. It was fun. A lesson that I've learned is that a
sewing machine works much better in the morning than late at night
when you are tired and sleepy. Really !

Other then that, it was a fun experience. Maybe I'll make another one
to commit this skill to mind. Maybe I'll run a kilt making workshop at
the next debconf :)


![Inside](/files/PA250002.JPG){: .img-thumbnail }
![Outside](/files/PA250003.JPG){: .img-thumbnail }


* [Attachment 0](/files/PA250002.JPG)
* [Attachment 1](/files/PA250003.JPG)
