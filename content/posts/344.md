Title: FLOSS crisis management software / community 
Slug: floss-crisis-management-software-community
Tags: Open World Forum
Date: 2010-10-12 15:23:31 CET+0000

The [second
workshop](http://www.openworldforum.org/attend/agenda/floss-against-crisis)
I attended at the open word forum was about the involvement of the
free and open source community in natural disasters. Quite frankly I
didn't know anything about these efforts and I was amazed by the level
of commitment of this community.

The first speaker introduced
[sahana](http://www.sahanafoundation.org/) that is a FOSS disaster
management system. The Wikipedia has a
[page](http://en.wikipedia.org/wiki/Sahana_FOSS_Disaster_Management_System)
about its story and development. Mark Prutsalis introduced the role of
free software and crowd-sourcing in recent years highlighting its
strengths and flexibility compared to government agencies. In
particular sahana and the people from crisis common played a big role
in the first days of the relief efforts during the Haiti earthquake
early this year.

Another important player is the [ushahidi](http://www.ushahidi.com/)
community. The build a number of tools that are increasingly used to
connect poeple during natural disasters, monitor political elections,
diseases, fires, etc. It's clear that build awareness among people and
empower them with these kind of tools can really give back to the
community a voice that was long lost in the global village. And there
are many projects that go in this direction.

On really important components of these technologies is
[openstreetmap](http://www.openstreetmap.org/). Differently from other
commercial map providers, and thanks to free licenses and open
standards, openstreetmap allowed to visualize and organize the
over-growing information that can be collected through crowd sourcing.
In this context open standard plays a really important role giving the
possibility to exchange information and to easily
[mashup](http://en.wikipedia.org/wiki/Mashup_%28web_application_hybrid%29)
new different sources in some useful way.

Another nice effort is the to build tools is lead by the [Crisis
Common](http://crisiscommons.org/) community. Last week they organized
a [Crisis Camp](http://www.barcamp.org/CrisisCamp-Paris-2) in Paris.
Unfortunately I could not attend, but I really applause the effort and
the spirit of such gatherings.

Last link I want to give is about [NGO in a
Box](http://ngoinabox.org). Despite not directly related to crisis
management and crowd sourcing, this kind of tools are certainly a
terrific help to the humanitarian aid community and it definitely
worth mention them.


