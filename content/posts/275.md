Title: rsync jail with rssh
Slug: rsync-jail-rssh
Tags: backup, rssh, rsync
Date: 2009-02-10 15:22:24 CET+0000

I just finished to setup our off-site backup . Since the admin in the
data center where out off-site backup machine is hosted asked for a
pull method, I had to setup a "secure" rsync jail to allow him/her to
move our data. To this end, I installed rssh, rsync and I setup a
simple jail for it.
<!--break-->

 The rssh configuration is pretty straightforward.  First you need to
add the backup user to your system.
```
adduser--disabled-login  --no-create-home --home /var/chroot/backup backup 
```
Then you can use the script that comes with rssh to build the jail.
This script actually build the jail only with scp. This is not enough
as we want rsync. To add ryync to the jail is necessary to copy the
binary and a couple of libraries that we can easily find out with ldd.
```
#/usr/share/doc/rssh/examples/mkchroot.sh /var/chroot
#cp /usr/bin/rsync  /var/chroor/usr/bin
#ldd `which rsync`
	libacl.so.1 => /lib/libacl.so.1 (0x00002ad794dd6000)
	libpopt.so.0 => /lib/libpopt.so.0 (0x00002ad794edc000)
	libc.so.6 => /lib/libc.so.6 (0x00002ad794fe5000)
	libattr.so.1 => /lib/libattr.so.1 (0x00002ad795222000)
	/lib64/ld-linux-x86-64.so.2 (0x00002ad794cbe000)
#cp /lib/libacl.so.1 /lib/libpopt.so.0 /lib/libattr.so.1 /var/chroot/lib
```
Then we need to configure rssh to allow the backup user in the jail
```
#cat /etc/rssh/rssh.conf
logfacility = LOG_USER
umask = 022
user=backup:011:10000:/var/chroot
```
In order to give read-only access to the user partition I decided to
bind mount the real partition readonly
```
#mount -o ro --bind /srv/backup /var/chroot/home/backup
```
and in the end to copy the public key of the backup user in the .ssh
directory.

I can finally rync my encrypted backup !

```
rsync --list-only backup@backupmachine: .
drwxr-xr-x        4096 2009/02/10 03:21:53 .
-rw-r--r--    63056911 2009/02/09 15:59:47 dev_full.1.dar
-rw-r--r--      214303 2009/02/09 16:11:46 dev_incr1.1.dar
-rw-r--r--      281236 2009/02/10 03:17:38 dev_incr2.1.dar
-rw-r--r--     3323334 2009/02/09 16:05:25 mail_full.1.dar
-rw-r--r--        9584 2009/02/09 16:14:19 mail_incr1.1.dar
-rw-r--r--  1211287691 2009/02/10 03:21:24 mail_incr2.1.dar
-rw-r--r--  1156597930 2009/02/09 16:06:48 nfs_full.1.dar
-rw-r--r--    33476937 2009/02/09 16:14:36 nfs_incr1.1.dar
-rw-r--r--   205390873 2009/02/10 03:21:51 nfs_incr2.1.dar
[...]
```



