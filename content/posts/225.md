Title: cduce mailing lists
Slug: cduce-mailing-lists
Tags: cduce
Date: 2008-02-06 16:08:13 CET+0000

Today I finished to migrate all cduce mailing list to pps.
Once I got the archive, it was fairly easy to recreate the html pages
(with the arch script in the mailman directory) and to subscribe
everybody to the new lists. The old sympa server and email address is
definitely gone.

I moved all mailing lists from *@cduce.org to *@mailman.cduce.org. All
information are here : http://www.cduce.org/contacts.html

Today I've also configured spamassassin + amavis-new + clamavd on our
mail server. Hopefully this will keep our lists clean.

I didn't test it properly, but if you are a spammer feel free to help
me :)

