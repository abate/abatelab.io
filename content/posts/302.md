Title: fosdem cool stuff
Slug: fosdem-cool-stuff
Tags: fosdem
Date: 2010-02-07 21:39:17 CET+0000

<img src="http://www.fosdem.org/promo/going-to"/>

Well... I went to fosdem 2010:) Very nice indeed as every year. Kudos
to the organizers. Even though this year I didn't manage to grab a
t-shirt as I usually do ...

##indenti.ca
I attended a presentation about identi.ca. I'm not very much
micro-blogging person. I like social networks but just to get in touch
with friends and nothing else. What I like about identi.ca (as many
other people at fosdem) is the FOSS side of it and it's implementation
of open standards. The talk was well delivered and informative. Thanks
!

##gwibber
So, during a presentation I snoop on my neighbor's laptop and he was
using twhirl, that is a nice "freeware" piece of software. Closed
source ? No way ! There is a nice FOSS alternative though. Gwibber.
I've tried it out yesterday and it does a nice job. From its homepage,
this is the description.

Gwibber is an open source microblogging framework and desktop client
for GNOME developed with Python and GTK+. The Gwibber backend is a
stand-alone daemon that manages updates and retrieves stream data from
social networks. The Gwibber backend can be accessed through D-Bus and
currently uses GConf to store account configuration info.

homepage: http://live.gnome.org/Gwibber

Does it work ? uhmmm playing with it I would say that is a still a bit
young. The interface does not always work (how do I replay to a
message, or how to I subscribe to a specific tag on identi.ca ?) and
it is not very well polished. A piece of software to be consider, but
not ready for prime time, at least for me.

##Nmap Scripting Engine (NSE)
I attended a very nice presentation about NSE, the nmap scripting
engine that I didn't know at all. It's a very powerful tool to scan an
analyzing networks. There is a free <a
href="http://nmap.org/book/nse.html">chapter</a> of the nmap book
available and I think it's completely worth reading it.

This is the <a
href="http://www.bamsoftware.com/talks/fosdem-2010.html"> material</a>
from the presentation that includes a very nice handout about nse.

##lua
And NSE is written in LUA that is a powerful, fast, lightweight,
embeddable scripting language. I've already came across it and I think
I'll spend sometimes to see if it can be useful for my projects.

homepage : http://www.lua.org/about.html

##Drupal
As every year I spent a few hours in the Drupal run. The drupal
community is extremely active. I failed to attend the talk about the
upcoming drupal 7 release. Fortunately I've found <a
href="http://blip.tv/file/2772239">this keynote </a> online that is
work watching if you are interested. Drupal 7 is going to be a super
release both from site designed and developers. I really looking
forward to it.

During a presentation about installing and developing with Drupal, the
discussions went on the dependency system of the modules and plugins
in Drupal. Needless to say that this might be a nice application of
the work we are doing with mancoosi. At it should also be reasonably
easy to integrate it with <a
href="http://drupal.org/project/drush">drush</a>. Now I just need a
php binding of a sat solver that understands CUDF. AH!

##guake
Guake is a quake console stile unix terminal for gnome. I'm addicted.
I've configured guake to slide down with alt+space, in the same way
I've ubiquity on firefox. I feel home. The console is there when I
need it, it's fast, and with the correct key-bindings is just like
gnome-terminal. I'm definitely happy I've discover it. Next step is
going to be a tiling window manager, but this is material for another
post.

this is the wikipedia page about it :
http://en.wikipedia.org/wiki/Guake
It's homepage seems down at the moment.

I attended many other presentations, but this is enough for one post.
See you at fosdem 2011

