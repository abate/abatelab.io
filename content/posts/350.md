Title: connected components with graphivz
Slug: connected-components-graphivz
Tags: ccomps, dot, graphivz, gvpr
Date: 2010-10-25 15:38:33 CET+0000

I just discovered the *gvpr* transformation language that comes with
graphivz. Up until now, I spend far too much time manipulating dot
graphs for various reasons.  *gvpr* is an awk like language to
manipulate dot graphs. It seems pretty complete, and allows you to do
a lot of simple operations in one line. One small example is to split
a graph in its connected components in many graphs, one per file.

The one line is :
```
ccomps -x dominators.dot | gvpr -f split.gvpr
```
where *ccomps* is a tool (that is also part of the graphivz suite)
that computes connected components of a dot graph. The option *-x*
creates a digraph for connected component (by default it creates a
graph with a lot of subgraphs). The results of this command is piped
to *gvpr* that is the graphivz language interpreter. The program
itself is very simple :
```
BEGIN { int n; }
BEG_G {
  n = nNodes($G);
  if ( n > 2 ) writeG($G,$G.name); 
}
END {}
```
for each graph, if the graph has more then two nodes, write the graph
in a file. and voila !


