Title: using apt-get and aptitude with cudf
Slug: using-aptget-and-aptitude-cudf
Tags: apt-get, aptitude, cudf, debian, mancoosi
Date: 2010-07-21 13:55:30 CET+0000

apt-get and aptitide were two missing competitors of the [misc
competition](http://mancoosi.org/misc-2010/). However it is important
and interesting how these two tools compete against other solvers
submitted to MISC. In this post I want to present two simple tools to
convert cudf documents to something that apt-get based tools can
handle. Cudf and debian share many characteristics but also have
important semantic differences. One important difference is about
installing multiple versions of the same package. Since this is
allowed in cudf, but not in debian, we can use apt-get and aptitude
only to solver cudf problems that respect this constraint, ruling out,
for example, all cudfs from the rpm world. Another difference to take
care is about the request semantic. In cudf, request can contain
version constraints. For example, one can ask to upgrade the package
```wheel``` to a version greater then 2. Since it is not possible to
translate directly this request in cudf we are forced to add a dummy
package encoding the disjunction of all packages that respect this
constraint. This problem does not arise with remove request as the
refer always to the currently installed package.

Apt-get needs two files : The Packages file that contains the list of
all packages known to the meta-installer and the status file that
contains the list of packages that must result currently installed. To
generate these files I wrote a small utility using the [dose3
framework](http://mancoosi.org/software/) imaginatively called
```cudftodeb``` . This tool gets a cudf and produces three files :
Packages, status and Request  with the Request file containing the
list of files to install or remove in a syntax compatible with apt-get
.

In other to run apt-get/aptitude with these files, you would need a
simple bash script. You can find details [here for
apt-get](http://chistera.yi.org/~adeodato/blog/106_fakeapt.html) and
[here for aptitude](http://mancoosi.org/~abate/fakeaptitude). Most
important option is the ```-s``` used to simulate an installation.

With the ```-v``` option of apt-get we can generate a parsable
solution. This output is the piped through an other tool called
```aptgetsolutions``` in order to produce a cudf solution closing the
circle.

For example, this is the trace produced by aptitude when trying to
solve the [legacy.cudf
problem](http://gforge.info.ucl.ac.be/plugins/scmsvn/viewcvs.php/trunk/dose3/libcudf/tests/legacy.cudf?revision=1659&root=mancoosi&view=markup)
:

```
Reading package lists...
Building dependency tree...
Reading extended state information...
Initializing package states...
Reading task descriptions...
The following NEW packages will be installed:
  bicycle dummy_wheel electric-engine{b} glass{a} window{a}
The following packages will be upgraded:
  door wheel
2 packages upgraded, 5 newly installed, 0 to remove and 1 not upgraded.
Need to get 0B of archives. After unpacking 0B will be used.
The following packages have unmet dependencies:
  gasoline-engine: Conflicts: engine which is a virtual package.
  electric-engine: Conflicts: engine which is a virtual package.
The following actions will resolve these dependencies:

     Remove the following packages:
1)     gasoline-engine

The following NEW packages will be installed:
  bicycle dummy_wheel electric-engine glass{a} window{a}
The following packages will be REMOVED:
  gasoline-engine{a}
The following packages will be upgraded:
  door wheel
2 packages upgraded, 5 newly installed, 1 to remove and 0 not upgraded.
Need to get 0B of archives. After unpacking 0B will be used.
WARNING: untrusted versions of the following packages will be installed!

Untrusted packages could compromise your system's security.
You should only proceed with the installation if you are certain that
this is what you want to do.

  wheel bicycle dummy_wheel door glass electric-engine window

*** WARNING ***   Ignoring these trust violations because
                  aptitude::CmdLine::Ignore-Trust-Violations is 'true'!
Remv gasoline-engine [1] [car ]
Inst bicycle (7 localhost) [car ]
Inst glass (2 localhost) [car ]
Inst window (3 localhost) [car ]
Inst door [1] (2 localhost) [car ]
Inst wheel [2] (3 localhost) [car ]
Inst dummy_wheel (1 localhost) [car ]
Inst electric-engine (1 localhost)
Conf bicycle (7 localhost)
Conf glass (2 localhost)
Conf window (3 localhost)
Conf door (2 localhost)
Conf wheel (3 localhost)
Conf dummy_wheel (1 localhost)
Conf electric-engine (1 localhost)
```

Not the package ```dummy_wheel``` used to encode the upgrade request
of ```wheel>>2```. This dummy package encodes the request as a
dependency :

```
Package: dummy_wheel
Version: 1
Architecture: i386
Depends: wheel (= 3)
Filename: /var/fakedummy_wheel1
```

One last remark about apt-get. I just run on this
[bug](http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=400768) today
using an old version of apt-get that is shipped with lenny. For our
experiments we are using only the latest version of apt-get in debian
testing.

