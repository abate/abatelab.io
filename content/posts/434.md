Title: Improved dose-builddebcheck
Slug: improved-dosebuilddebcheck
Tags: buildcheck, debian, dose3
Date: 2012-10-15 17:26:24 CET+0000

The new release of dose, apart from a few bug fixes, ships a new and
improved version of **dose-builddebcheck** ([man
page](http://dose.gforge.inria.fr/doc/man/buildcheck.html)). All the
improvements done to dose-builddebcheck are from a set of patches
submitted by [Johannes Schauer](http://blog.mister-muffin.de/) in the
context of the
[Bootstrap](http://wiki.debian.org/DebianBootstrap/TODO)
[GSoC](http://wiki.debian.org/SummerOfCode2012/Projects#Port_bootstrap_build-ordering_tool).
We are still actively working on this. I invite you to read josh's
recent [blog
post](http://blog.mister-muffin.de/2012/09/28/visit-at-paris-irill/)
on the topic. For more background and discussion, you can have a look
at the [archives of this mailing
list](http://lists.mister-muffin.de/cgi-bin/mailman/listinfo/debian-bootstrap).

Dose-builddebcheck (or buildcheck for short) is similar in intent to
dose-debcheck but for source packages. Buildcheck allows you to check,
just by looking at the *Source* and *Packages* files, if all the
dependencies can be satisfied and installed, all this in a matter of
seconds (less then 30 secs to test all the source packages in sid on
my laptop).

A simple example :

```
deb-buildcheck.native \
  --deb-native-arch=amd64 \
  tests/DebianPackages/Sid-amd64-Packages-050812.bz2 
  tests/DebianPackages/Sid-Sources-050812.bz2

native-architecture: amd64
background-packages: 55945
foreground-packages: 18175
broken-packages: 64

real	0m22.223s
user	0m21.937s
sys	0m0.204s
```

This will check for all source packages in the Source file if their
build dependencies can satisfied on amd64 given the Packages binary
file. This is nice but nothing new. A script based on the old edos
tools has been around for quite a long time. We want more !

The new exciting feature brought by Johannes is the capability of
checking source packages for cross-compilation :

```

dose-builddebcheck -f -e -s --checkonly picolisp \
  --deb-native-arch=amd64 \
  --deb-foreign-archs=armel \
  --deb-host-arch=armel \
  tests/DebianPackages/Sid-amd64-Packages-050812.bz2 \
  tests/DebianPackages/Sid-armel-Packages-050812.bz2 \
  tests/DebianPackages/Sid-Sources-050812.bz2 

(I)Sources: Parsing Sources file tests/DebianPackages/Sid-Sources-single-version-050812.bz2...
(I)Format822: total packages 17723
(I)Format822: Merging repositories
(I)Packages: Parsing Packages file tests/DebianPackages/Sid-armel-Packages-050812.bz2...
(I)Packages: Parsing Packages file tests/DebianPackages/Sid-amd64-Packages-050812.bz2...
(I)Format822: total packages 58095

native-architecture: amd64
foreign-architecture: armel
host-architecture: armel
report:
 -
  package: src:apicolisp
  version: 3.1.0.7-1
  architecture: kopensolaris-amd64,solaris-amd64,amd64,any-i386,any-armel,any-armeb,any-arm,any-avr32,any-hppa,any-m32r,any-m68k,any-mips,any-mipsel,any-powerpc,any-s390,any-sh3,any-sh3eb,any-sh4,any-sh4eb,any-sparc,any-armhf
  source: picolisp (= 3.1.0.7-1)
  status: ok
  
background-packages: 75818
foreground-packages: 17723
broken-packages: 0
```

This will check, if the source package 'picolisp' in the *Sources*
file can be cross compiled for 'armel' on the native architecture
'amd64' given the list of binary packages in the *Packages* file. The
generated report is, as for dose-debcheck, encoded in yaml and it can
be simply parsed using an off-the-shelf library.

Apt is the canonical tool that can be used to check if a package can
be cross compiled.
Josh found a few discrepancies between dose and apt results. This was
a very good test for both tools: Bug
[#683786](http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=683786) is
a very interesting read...

Dose 3.1.1 (the latest release) should land soon in experimental.
Otherwise you can get it from our [(new) project
homepage](https://gforge.inria.fr/projects/dose/).


