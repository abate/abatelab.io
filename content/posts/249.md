Title: lvm and friends
Slug: lvm-and-friends
Tags: lvm, xfs
Date: 2008-07-11 15:03:20 CET+0000

Not a tutorial, just few lines to write down what I've done.

This command gives you information about all virtual groups
```
vgdisplay -v
```

Then, to create a logical group :
```
lvcreate -l 59581 -n data vg1
```

the ``` -l ``` option specifies the number of extent to allocate for
this lv . You can see the number of available extents for a vg with
the command above. This is useful to know when you want to create a lv
using all the available space.

Then let's create an xfs fs on this lv :
```
mkfs.xfs -l logdev=/dev/vg1/log1,size=128m -f /dev/vg1/data
```
I'm not going to specify any other esoteric settings to build the fs.
The only important option here is to specify external log partition.

To mount the partition we need to give a log list of parameters.
```
mount -t xfs /dev/vg1/data ex -o noatime,nodiratime,nobarrier,logbufs=8,logdev=/dev/vg1/log1
```

There are a lot of threads around. This is a good pointer.

http://thias.marmotte.net/archives/2008/01/05/Dell-PERC5E-and-MD1000-performance-tweaks.html

Next jumbo frames !

