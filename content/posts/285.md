Title: storm, sqlite and string representation 
Slug: storm-sqlite-and-string-representation
Tags: sql, sqlite3, storm
Date: 2009-03-27 14:13:46 CET+0000

Arghhh today I've discovered reading <a
href="https://bugs.launchpad.net/storm/+bug/293483:> this bug
report</a> that specifying strings are ```RawStr()``` in strom, they
are actually stored as blobs in sqlite3. The very bad side-effect is
that string comparison does not work !!!

The right way to store strings with storm is to use the
```Unicode()``` data type instead and to wrap all your strings with
the ```unicode``` function. If you need "utf-8", you can pass it as
optional argument to it. Now string comparisons are 10 times faster
!!!!!!!! Argggg




