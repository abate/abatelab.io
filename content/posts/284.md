Title: add sqlite3 collation with python 2.5 and django
Slug: add-sqlite3-collation-python-25-and-django
Tags: django, python, sqlite3
Date: 2009-03-24 18:56:00 CET+0000

a while ago <a
href="https://mancoosi.org/~abate/add-sqlite3-collation-with-python-25-and-storm">I
wrote about enabling the sqlite3 extension with storm</a> . This is
how you do it with the Django ORM. The collation is the same and all
details are in the old post. The only tricky part is to establish the
connection with ```cursor = connection.cursor()``` before calling the
function to enable the extension. Failing to do so, will result in an
error as the connection object will be null.

```python
    def add_collation():
        from django.db import connection
        import sqlitext
        cursor = connection.cursor()
        sqlitext.enable_extension(connection.connection,1)
        cursor.execute("SELECT load_extension('sqlite/libcollate_debian.so')")
```

