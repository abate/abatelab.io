Title: virtualbox + kernel image 3.16.0-4 on debian unstable
Slug: virtualbox-kernel-image-3-16-0-4-on-debian-unstable
Date: 2017-09-22 20:01:35
Tags: debian

Recently I got stuck with the problem of using an old kernel (now in jessie)
on a new machine running unstable. Since the kernel in question is still available
and supported this is not a problem per se, but if you try to recompile
the dkms modules for virtualbox then the kernel headers are nowhere to be found.
To solve this problem we need to first add these two line in  apt/sources.list

```
deb http://ftp.fr.debian.org/debian jessie main contrib non-free
deb http://security.debian.org/ jessie/updates main
```

and then nicely asking apt-get to install the needed gcc version from jessie's archive.
This is the less distractive way of doing this.

After this simple operation I can now recompile the virtualbox kernel modules.


```
$sudo apt install linux-headers-3.16.0-4-amd64 gcc-4.8/jessie
Reading package lists... Done
Building dependency tree       
Reading state information... Done
Selected version '4.8.4-1' (Debian:8.9/oldstable [amd64]) for 'gcc-4.8'
The following packages were automatically installed and are no longer required:
  libgsoap-2.8.49 libvncserver1
Use 'sudo apt autoremove' to remove them.
The following additional packages will be installed:
  cpp-4.8 gcc-4.8-base libasan0 libgcc-4.8-dev libisl10 linux-compiler-gcc-4.8-x86 linux-headers-3.16.0-4-common
  linux-kbuild-3.16
Suggested packages:
  gcc-4.8-locales gcc-4.8-multilib gcc-4.8-doc libgcc1-dbg libgomp1-dbg libitm1-dbg libatomic1-dbg libasan0-dbg libtsan0-dbg
  libquadmath0-dbg
The following NEW packages will be installed:
  cpp-4.8 gcc-4.8 libasan0 libgcc-4.8-dev libisl10 linux-compiler-gcc-4.8-x86 linux-headers-3.16.0-4-amd64
  linux-headers-3.16.0-4-common linux-kbuild-3.16
The following packages will be DOWNGRADED:
  gcc-4.8-base
0 upgraded, 9 newly installed, 1 downgraded, 0 to remove and 44 not upgraded.
Need to get 17.3 MB of archives.
After this operation, 63.2 MB of additional disk space will be used.
Do you want to continue? [Y/n] 
[...]
```

