Title: welcome to the brave new world of git-svn
Slug: welcome-brave-new-world-gitsvn
Tags: git, svn
Date: 2010-09-09 09:23:38 CET+0000

I'm definitely fed up with the lack of feature of svn. I always end up
making backup copies of files, committing the wrong patch set, being
unable to cherry-pick what to commit, not to talk about branching,
merging and other marry activities. So today I overcome my laziness
and setup a git svn repository for dose3 (since it's all there, go
ahead, and be happy!).

The concept of git svn if pretty easy. You work with git, and from
time to time, you commit in svn. Best of both worlds, and ppl won't
even notice that you are not using their pet SCM, apart for the happy
look of your face :)

First let's clone the svn repo :
```
git svn clone https://gforge.info.ucl.ac.be/svn/mancoosi/trunk/dose3
```

This will probably hangup if you have big files in the svn repo. I
don't think it's a problem with svn itself, but more likely a problem
with the forge at ucl.ac.be. Anyway, if this happen, just go in the
dose3 directory and finish it up with a ```git svn fetch```

Now it's time to sync all you changes and test from your old svn tree.
I choose to rsync everything away :
```
rsync -avz --exclude=.svn --exclude=_build ../mancoosi-public/trunk/dose3/ .
```
of course we want to exclude all the compilation leftovers and .svn
directories ...

Ahhhhhhh freedom. Now I can edit, stash away changes, commit,
uncommit, rebase, cherry-pick... I feel home.

At the end of the day we have to check if somebody updated the svn
repo with ```git svn rebase```, maybe resolve a couple of conflicts
and then I can commit all my work with ```git svn dcommit```.

I've been working with git svn only for a couple of days now, but I
feel it pretty stable and trustworthy.

