Title: nagios
Slug: nagios
Tags: 
Date: 2008-03-19 17:48:34 CET+0000

This week I spent some time configuring nagios to track our
infrastructure, including web services and hardware.

Nagios is an interesting piece of software. It's very flexible and
kinda easy to setup. I've to say that the documentation is not
particularly well written. However sites like
http://www.nagiosexchange.org/ make like very easy. Setting it up on
debian is pretty straightforward.

It took me a while to understand how the various configuration files
are organized and what is the object hierarchy. My small contribution
today is about this plugin to check if a web service is up and
running.

you can put this snippet in /etc/nagios-plugins/config/url.cfg
```
define command{
        command_name    check_url
        command_line    /usr/lib/nagios/plugins/check_http -H $HOSTADDRESS$ -I $HOSTADDRESS$ -u $ARG1$
}

define command{
        command_name    check_url_ssl
        command_line    /usr/lib/nagios/plugins/check_http -H $HOSTADDRESS$ --ssl -u $ARG1$
}
```

Then to use it, you have to add a file (eg. web_services.conf) in
/etc/nagios2/conf.d with this content:

```
define service {
        host_name                       WebService
        service_description             PYTHON
        check_command                   check_url_ssl!https://trac.cduce.org/
        use                             generic-service
        notification_interval           0
}
```

to check if a particular service is there. The trick is to define a
host named WebService to organize all service under one heading.

Next Step is to integrate munin with nagios.

