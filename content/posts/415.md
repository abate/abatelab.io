Title: How to convince cupt and smartpm to ignore the gpg signature of a Release file
Slug: how-convince-cupt-and-smartpm-ignore-gpg-signature-release-file
Tags: cupt, debian, hacking, smartpm
Date: 2012-05-18 14:06:19 CET+0000

I'm blogging about this small configuration issue as it took me some
time to figure out how to configure cupt and smart to solve this
problem. The reason I'm playing with cupt and smartpm is that I'm
working to compare
[again](http://mancoosi.org/~abate/package-managers-comparison) a
number of package managers in debian against the state of the art cudf
solvers using
[mpm](http://mancoosi.org/~abate/mpm-putting-all-pieces-together), and
I'm suffering quite a bit to configure my virtual environment. Last
year I promised to revise and fix our results. I didn't forget my
promise, but it seems it took longer then expected.

Anyway, back to the main topic. The *release-problem* arises because
the key used to sign *sarge* (that I'm using as baseline for my
experiments) is long expired. If you try to retrieve sarge from
archive.debian.org you will find that sarge is signed with the key
A70DAF536070D3A1 and apt-get will complain loudly if you try to use an
archive signed with an expired key.

##cupt

For cupt this is documented in the man page and there are a number of
options to add either to */etc/apt/apt.conf* to the the cupt own conf
file. Then cupt will happily accept the sarge Packages file and let
you run update.

```
cupt::cache::release-file-expiration::ignore "true";
cupt::update::check-release-files "false";
cupt::update::keep-bad-signatures "true";
```

##smartpm

For smart I could not found this information anywhere, but reading the
source code (tnx good it's python !). To cut it short, you need to set
the key *trustdb* to an empty value for a specific channel. On the
command line you get something like :

```smart channel --set  aptsync-614482cb2c7e08d5722af3498232ba52
keyring= --config-file=/root/var/lib/smart/config```

where *aptsync-614482cb2c7e08d5722af3498232ba52* is the channel name
corresponding to sarge in my conf. Since I'm using a simulated
environment, I save the result of this option in a non-default config
file in my chroot.


