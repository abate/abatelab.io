Title: rssh and suid mode
Slug: rssh-and-suid-mode
Tags: debian, dpkg, rssh
Date: 2010-06-23 12:25:00 CET+0000


I use rssh to allow restricted shell access to my servers. a few weeks
ago I've noticed a lot of errors in my log of the form
```
Jun 22 11:04:06 dev rssh[13508]: setting log facility to LOG_USER
Jun 22 11:04:06 dev rssh[13508]: setting umask to 022
Jun 22 11:04:06 dev rssh[13508]: line 38: configuring user xxxx
Jun 22 11:04:06 dev rssh[13508]: setting xxxx's umask to 022
Jun 22 11:04:06 dev rssh[13508]: allowing rsync to user xxxx
Jun 22 11:04:06 dev rssh[13508]: chrooting xxxx to /home/xxxx
Jun 22 11:04:06 dev rssh[13508]: chroot cmd line: /usr/lib/rssh/rssh_chroot_helper 5 "rsync --server --sender -lde.L . "
Jun 22 11:04:06 dev rssh_chroot_helper[13508]: new session for xxxx, UID=1000
Jun 22 11:04:06 dev rssh_chroot_helper[13508]: chroot() failed, 5: Operation not permitted
```

It turned out a problem with a recent security update that removed the
set user id from ```/usr/lib/rssh/rssh_chroot_helper```. Dpkg has a
nice way to make permanent such changes, that is
```dpkg-stateoverride```. It simply boils down to:

```
#dpkg-statoverride --add root root 4755 /usr/lib/rssh/rssh_chroot_helper
#dpkg-statoverride --update
# stat  /usr/lib/rssh/rssh_chroot_helper
  File: `/usr/lib/rssh/rssh_chroot_helper'
  Size: 24904     	Blocks: 56         IO Block: 4096   regular file
Device: 801h/2049d	Inode: 21326498    Links: 1
Access: (4755/-rwsr-xr-x)  Uid: (    0/    root)   Gid: (    0/    root)
Access: 2010-06-22 12:03:17.170460092 +0200
Modify: 2010-04-04 02:07:37.000000000 +0200
Change: 2010-06-22 12:02:23.040115785 +0200
```

