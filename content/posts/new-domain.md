Title: New Domain and new Professional life
Date: 2020/02/16

After 3 long years of procrastination, yesterday I finally motivated myself to
resurrect this blog roll. Migrating the content with [Pelican][1] was a breeze.
I've been using this system quite a while later, and I'm pretty happy about it.
And adding everything on [pages.gitlab.io][2] was also relatively easy.

In the last three years, I started working at [Nomadic Labs][3], a French
company working on [Tezos][4] development. At NL I work on the development
of the node, enjoy teaching during our [regular training sessions][4], and
in general learning about the blockchain world.

[1]: https://blog.getpelican.com/
[2]: https://about.gitlab.com/stages-devops-lifecycle/pages/
[3]: https://nomadic-labs.com
[4]: https://training.nomadic-labs.com
