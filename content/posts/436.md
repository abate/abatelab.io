Title: dose has a new git repository and mailing list !
Slug: dose-has-new-git-repository-and-mailing-list
Tags: debian, dose3, ocaml
Date: 2012-10-14 16:20:05 CET+0000

Recently we did a bit of clean up of our git repositories and now
thanks to [roberto's](http://www.dicosmo.org/index.html.en) efforts we
have a new shiny [git
repository](https://gforge.inria.fr/scm/browser.php?group_id=4395) on
the inria forge and two mailing lists to discuss development and user
questions.

If you are a user, or interested in dose development, please sign up
to these mailing lists:

* dose-discus :http://lists.gforge.inria.fr/cgi-bin/mailman/listinfo/dose-discuss
* dose-devel  : http://lists.gforge.inria.fr/cgi-bin/mailman/listinfo/dose-devel

if you already have a copy of the latest git repository, you can
change the upstream repository issuing the following command :
```
     git config remote.origin.url git://gforge.inria.fr/dose/dose.git
```

If you are curious, you can clone dose ```git clone
git://gforge.inria.fr/dose/dose.git``` and let us know what you think
about it.

The API documentation is available
[here](http://dose.gforge.inria.fr/doc/api/). The man pages or various
applications developed on top of the dose library are available
[here](http://dose.gforge.inria.fr/doc/man/). We are still actively
working on the documentation and any contribution is very much
welcome. I'm working on a nice homepage...

You can get the latest tar ball release here :
https://gforge.inria.fr/frs/?group_id=4395
Old releases will be left on the [old
forge](http://gforge.info.ucl.ac.be/frs/?group_id=35).

##Update

And now we are even **social** ! follow us on identi.ca :
http://identi.ca/group/dose3

