Title: svn wrapper / umask 002
Slug: None
Tags: None
Date: 2009-02-05 10:35:16 CET+0000


As it is explained in the svn book [1] in order to share a repository
in a multi user environment, every user should set their umask to 002,
to avoid permission problems. Implementing this in debian can be
problematic as explained in this bug report [2]. Setting this umask by
default is a potential security risk and should be avoided (this is
true at least in redhat where every user is member of the user group,
in debian we have a better approach where every user a member of of
its own user group by default). The solution is to use a wrapper and
to put it in ```/usr/local/bin``` . The wrapper is actually already
being written for you. This is what you have to do:

```
apt-get install subversion-tools
ln -s /usr/bin/svnwrap /usr/local/bin/svn
ln -s /usr/bin/svnwrap /usr/local/bin/svnserve
```

since in debian by default ```/usr/local/bin``` is looked up before
then ```/usr/bin``` the two links created above are used instead of
the real svn binaries avoiding any problem related to upgrades.

[1] http://svnbook.red-bean.com/en/1.0/ch06s05.html
[2] http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=24236

