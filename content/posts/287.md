Title: sharing a svn repo
Slug: sharing-svn-repo
Tags: svn
Date: 2009-04-03 08:17:48 CET+0000

Recently I had few problems with a svn repository that is shared
between multiple ssh users. I followed the instructions <a
href="http://svnbook.red-bean.com/en/1.4/svn.serverconfig.multimethod.html">in
the svn book</a> and to solve the problem once for all I recreated the
repo from scratch. Briefly:

```
svnadmin dump SVN > /tmp/dump
mv SVN SVN-old
svnadmin create SVN
chmod -R g+rw SVN
find SVN -type d -exec chmod g+s {} \;
chown -R root.cvs SVN
 svnadmin load SVN < /tmp/dump
```

hopefully this is gonna work. hopefully, otherwise I guess I missing
something that is very basic !



