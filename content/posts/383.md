Title: Eating my own dog food - mpm
Slug: eating-my-own-dog-food-mpm
Tags: debian, mancoosi, mpm
Date: 2011-05-13 10:37:59 CET+0000

After a bit of work, today I decided to start using mpm, the mancoosi
package manager, to upgrade my laptop. My first use of it on a
production system - until now I run all my experiments in throw-away
virtual machines - and it works !

Not rocket science here. During the last month David Kalnischkies  (of
APT fame) visited our offices in Paris and together with zack worked
out a communication protocol between apt-get and the mancoosi cudf
solvers (EDSP). I guess somebody is going to announce all details
about this endeavor soon. This cooperation enabled us to advance in
the integration of apt-get and the mancoosi technology.

Reusing the same protocol, and backend I developed to translate the
apt problem to cudf (and to call a suitable solver), I've re-wrote
large part of mpm and added the possibility to [generate the
installation
plan](http://mancoosi.org/~abate/aptget-installation-plan) before
calling dpkg and really installing the selected packages.

Below notice the intermediate calls as *Inject Model *, *Simulate*,
*Compare Models* . These are at the moment stubs that are going to
call the simulation framework developed at mancoosi.

The food :

```
abate@zed.fr:~/Projects/git-svn-repos/mpm$sudo ./mpm.py -c mpm.conf update
Reading package lists... Done
Building dependency tree       
Reading state information... Done
Inject Model ...


abate@zed.fr:~/Projects/git-svn-repos/mpm$sudo ./mpm.py -c mpm.conf upgrade
Reading package lists... Done
Building dependency tree       
Reading state information... Done
The following new packages will be installed:
libtracker-client-0.8-0 libnet-ip-perl libio-socket-ssl-perl libasyncns0 libapr1 
5 to install
Simulate
Proceed ? yes/[no] 
yes
Reading package fields... Done
Reading package status... Done
Retrieving bug reports... Done
Parsing Found/Fixed information... Done
Reading changelogs... Done
apt-listchanges: Mailing root: apt-listchanges: changelogs for zed
Reading package fields... Done
Reading package status... Done
Retrieving bug reports... Done
Parsing Found/Fixed information... Done
Reading changelogs... Done
apt-listchanges: Mailing root: apt-listchanges: changelogs for zed
(Reading database ... 179534 files and directories currently installed.)
Preparing to replace libapr1 1.4.2-8 (using .../libapr1_1.4.4-1_amd64.deb) ...
Unpacking replacement libapr1 ...
Preparing to replace libasyncns0 0.8-1 (using .../libasyncns0_0.8-2_amd64.deb) ...
Unpacking replacement libasyncns0 ...
Preparing to replace libio-socket-ssl-perl 1.40-1 (using .../libio-socket-ssl-perl_1.43-1_all.deb) ...
Unpacking replacement libio-socket-ssl-perl ...
Preparing to replace libnet-ip-perl 1.25-2 (using .../libnet-ip-perl_1.25-3_all.deb) ...
Unpacking replacement libnet-ip-perl ...
Preparing to replace libtracker-client-0.8-0 0.8.17-2 (using .../libtracker-client-0.8-0_0.8.18-1_amd64.deb) ...
Unpacking replacement libtracker-client-0.8-0 ...
Processing triggers for man-db ...
Setting up libapr1 (1.4.4-1) ...
Setting up libasyncns0 (0.8-2) ...
Setting up libio-socket-ssl-perl (1.43-1) ...
Setting up libnet-ip-perl (1.25-3) ...
Setting up libtracker-client-0.8-0 (0.8.18-1) ...
localepurge: Disk space freed in /usr/share/locale: 0 KiB
localepurge: Disk space freed in /usr/share/man: 0 KiB
localepurge: Disk space freed in /usr/share/gnome/help: 0 KiB
localepurge: Disk space freed in /usr/share/omf: 0 KiB

Total disk space freed by localepurge: 0 KiB

localepurge: Disk space freed in /usr/share/locale: 0 KiB
localepurge: Disk space freed in /usr/share/man: 0 KiB
localepurge: Disk space freed in /usr/share/gnome/help: 0 KiB
localepurge: Disk space freed in /usr/share/omf: 0 KiB

Total disk space freed by localepurge: 0 KiB

Inject Model ...
Compare Models ...
abate@zed.fr:~/Projects/git-svn-repos/mpm$

```

Reg the installation plan, this is an xml file that will be passed to
a simulator developed by the university of L'Aquila to make sure that
the installation script (well, a model of them), will not cause any
problem during the installation.

The format is a very simple xml file as follows :
```xml
<selectionStates>
 <selectionState type="Install"> 
  <param name="package" value="libapr1" />
  <param name="version" value="1.4.4-1" /> 
  <param name="architecture" value="amd64" />
 </selectionState>
 <selectionState type="Install">
  <param name="package" value="libasyncns0" />
  <param name="version" value="0.8-2" />
  <param name="architecture" value="amd64" />
 </selectionState>
 <selectionState type="Install">
  <param name="package" value="libio-socket-ssl-perl" />
  <param name="version" value="1.43-1" />
  <param name="architecture" value="all" />
 </selectionState>
[...]
```

Soon APT will ship a patch to use the very same infrastructure of mpm
(yeii !!!). This will on one hand make mpm useless as package manager
on its own. It is a simple hack in python and I never tough to compete
with its big brothers. On the other hand I think it will stand as a
nice workbench to experiment with new ideas, to prototype new features
and to make it easier for poeple that are not c++ experts to play with
the APT library (thanks to python-apt) in a semi structured
environment. The code is in the mancoosi svn if you want to have a
look.

