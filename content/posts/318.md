Title: ocaml bindings for Buddy BDD
Slug: ocaml-bindings-buddy-bdd
Tags: bdd, ocaml
Date: 2010-04-13 16:08:27 CET+0000

I've taken some time off to finish the ocaml bindings to [Buddy
BDD](http://sourceforge.net/projects/buddy/develop).

This code is a fork of the bindings downloaded from
http://nicosia.is.s.u-tokyo.ac.jp/members/hagiya/xml/readme.htm#buddy
.The original authors are Akihiko Tozawa and Masami Hagiya and they
granted me - via personal communication - the right to release the
software
as open source.

I've added documentation to the .mli (mostly from the [buddy
website](http://buddy.sourceforge.net/manual/main.html)), few
functions that were missed in the original work, the build system and
an example.

I've released the code under GPLv3 . The code is available on
[github](http://github.com/abate/ocaml-buddy). This snapshot compiles
cleanly on debian unstable with the package libbdd-dev installed.
There is a micro example in the source. I plan to use the bindings
with some real world data in the next few weeks. Debian package is
work in progress.

Enjoy. As always feedback is welcome.

