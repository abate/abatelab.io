Title: gammu x301
Slug: gammu-x301
Tags: gammu, x301
Date: 2010-03-10 10:32:48 CET+0000

The other day I played a bit with [gammu](http://www.gammu.org) on my
laptop. The integrated modem is an Ericsson F3507g. There are a lot of
[useful
info](http://www.thinkwiki.org/wiki/Ericsson_F3507g_Mobile_Broadband_Module)
about this modem. This is just a resume different info I found on the
net.

First you would need to install gammu, that is packaged for debian, so
no prob. The to connect to the modem you would need a file
```.gammurc``` in you home directory (or in /etc) and the  correct
permission to talk to the modem. This is how my conf file looks like:
```
[gammu]
port = /dev/ttyACM0
connection = at19200
```

To enable the modem, if it is not already enabled at boot, you will
have to switch it on then then initialize it.
```
echo 1 > /sys/devices/platform/thinkpad_acpi/wwan_enable
/usr/sbin/chat -v "" "AT+CFUN=1" "+PACSP0" "AT" "OK" > /dev/ttyACM2 <  /dev/ttyACM2 
```

And this is it. Gammu is ready to talk to the modem and tell me few
info :

```
zed:~# gammu --identify
Device               : /dev/ttyACM0
Manufacturer         : Ericsson
Model                : unknown (F3507g)
Firmware             : R1B/1
IMEI                 : xxxxxxxxxxxxxxxxxxxx
SIM IMSI             : xxxxxxxxxxxxxxxxxxx
```

The [ref man of
gammu](http://www.gammu.org/wiki/index.php?title=Gammu:Full_reference_manual&action=purge)
is something nice to have at hand ...

Unfortunately I've realized that the SIM card I'm using is not able to
register to the net...
```
zed:~# gammu --networkinfo
Network state        : registration to network denied
GPRS                 : detached
```

Digging a bit more, this is can be seen directly talking AT with the
modem:
```
AT+CREG=2
OK
AT+CREG?
+CREG: 2,3
```
This page details the [list of AT
commands](http://www.shapeshifter.se/2008/04/30/list-of-at-commands/)
you can use.
The 3 part in ```+CREG: 2,3``` means "Registration denied" ... sigh,
sime to find a new sim...


