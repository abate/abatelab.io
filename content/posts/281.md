Title: my freerunner is finally free!!
Slug: my-freerunner-finally-free
Tags: freerunner
Date: 2009-03-21 11:52:51 CET+0000


This week I got myself a sim card and I started doing a more
appropriate use of my freerunner. For a start I've flushed it with the
latest SHR distribution. The SHR is based on FSO and I've the feeling
that is stable enough for daily use. After struggling a bit I've also
installed debian. The installer script is still a bit broken and, in
particular in this period, is very fragile because of the state of
flush of debian unstable. Nonetheless, after a couple of restart I
managed to get it going.

I've also flushed the FR with QI, the new boot loader, that is seems
faster then u-boot. It is also more strict about the kernel location
and naming, but nothing I can't handle :)

Last but not least, I flushed the new GSM firmware using the image
that is provided by OM. It worked straightaway, nothing much to report
here.

All in all, I'm pretty happy with the state of things. I received and
made phone calls as well as sms. Sometimes there are small usability
quirks that make it more difficult to use the phone that what it
 should. I've seen a lot of bug reports and suggestions and I'm sure
we're going to solve everything
soon.

Not very much of a technical post. Maybe I'll collect few links later
on...

