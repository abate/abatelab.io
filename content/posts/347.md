Title: quote of the day
Slug: quote-day-0
Tags: quotes
Date: 2010-10-15 10:58:38 CET+0000


" I know you think you understand what you thought I said but I'm not
sure you realize that what you heard is not what I meant"
— Alan Greenspan

