Title: mpm : putting all the pieces together 
Slug: mpm-putting-all-pieces-together
Tags: debian, mancoosi, mpm
Date: 2011-06-20 09:08:38 CET+0000

Recently, all relevant packages needed to run mpm (the mancoosi
package manager) landed in debian (thanks ralf and zack !). Now it
should be a tad easier to run mpm and to play with it. The code of mpm
is available on the [mancoosi svn
repository](http://gforge.info.ucl.ac.be/plugins/scmsvn/viewcvs.php/trunk/updb/mpm/?root=mancoosi)
(user/pass : mancoosi/mancoosi) . To run it, you also need to install
python-apt.

These are all cudf solvers you can use as mpm backends. They use
different solving techniques and were implemented for the [MISC
competition](http://www.mancoosi.org/misc-2011/index.html) :
* [mccs](http://packages.debian.org/sid/mccs)
* [packup](http://packages.debian.org/sid/packup)
* [aspcud](http://packages.debian.org/sid/aspcud)

In addition, you need the CUDF solver integration for APT / MPM , that
is the component in charge of translating the apt/mpm installation
problem to cudf and to provide a solution back to apt/mpm
* [apt-cudf](http://packages.debian.org/experimental/apt-cudf)

The last (optional) component is the dudf-save that allows you to
report an installation problem back to us .
mancoosi contest (dudfsave) is avalaible for download at
http://mancoosi.debian.net/

the mpm command line is still not very user friendly, but since it is
not meant to be used by final users or production systems, I think
I'll not spend too much time to add all bells and whistles.

enjoy.

