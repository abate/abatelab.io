Title: Package Managers Comparison - take 2
Slug: package-managers-comparison-take-2
Tags: apt-get, aptitude, competition, cupt, debian, mancoosi, misc, mpm, smartpm
Date: 2012-09-04 11:27:37 CET+0000

On year ago, we (the mancoosi team) published [a comparison
study](http://mancoosi.org/~abate/package-managers-comparison)
regarding the state of the art of dependency solving in debian. As few
noticed, the data presented had [few
glitches](http://mancoosi.org/~abate/package-managers-comparison#comment-193)
that I promised to fix. So we've repeated our tests using exactly the
same data we used one year ago, but now using the latest available
versions of all package managers as available in debian unstable.

During the last year, three out of four solver that we evaluated
release a major upgrade so I expected many improvements in
performances and accuracy.

* apt-get 0.8.10 -> 0.9.7
* aptitude 0.6.3 -> 0.6.7
* smart 1.3-1 -> 1.4
* cupt 1.5.14.1 -> 2.5.6

[Mpm](http://mancoosi.org/~abate/bypassing-aptget-solver), our
test-bench for new technologies,  changed quite a bit under the wood
as a consequence of the evolution of apt-cudf and the recent work done
in
[apt-get](http://mancoosi.org/~abate/aptget-external-solvers-call-testers)
to integrate external cudf dependency solvers.

Overall the results of our study are not changed. All solvers but mpm,
that is based on aspcud, are not scalable as the number of packages
(and alternatives) grows. It seems that **Smart** is the solver that
does not give up, incurring in a timeout (fixed at 60 seconds) most of
the time. **Aptitude** is the solver that tried to give you a
solution, doesn't matter what and as result providing solutions that
do not satisfy the user request for one reason or the other.
**Apt-get** does surprisingly well, but it gives up pretty often
showing the incomplete nature of it's internal solver. **Cupt**
sometimes timeout, sometimes gives up, but when it is able to provide
an answer it is usually optimal and it is very fast ... **Mpm**
consistently finds an optimal solution, but sometimes it takes really
a long time to do it. Since mpm is written in python and not optimized
for speed this is not a big problem for us. The technology used by mpm
is [now integrated in
apt-get](http://mancoosi.org/~abate/aptget-external-solvers-call-testers)
and I hope this will alleviate this problem.

All the details of our study can be found one the [Mancoosi
Website](http://www.mancoosi.org/measures/packagemanagers/2012/) as
usual with a lot of details. For example here you can find the results
when mixing four major releases :
[sarge-etch-lenny-squeeze](http://www.mancoosi.org/measures/packagemanagers/2012/logs.sarge-etch-lenny-squeeze/).

Comments are more then welcome.


