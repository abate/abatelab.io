Title: Mini Debian Conf 2012 in Paris : Bootstrapping Debian for a new architecture
Slug: mini-debian-conf-2012-paris-bootstrapping-debian-new-architecture
Tags: debian
Date: 2012-11-25 11:00:00 CET+0000

I just finished to address the awesome debian crowd at the [Mini Deb
conf](http://fr2012.mini.debconf.org/) in paris. My presentation was
about a few challenges we have ahead to bootstrap debian on a new
architecture. [Johannes Schauer](http://blog.mister-muffin.de/) and
[Wookey](http://wookware.org/) did a lot of work in the last few
months particularly focusing on Linaro/Ubuntu. After Wheezy I think it
is important to catch up with their work and integrate it in debian.

The two main take away messages from my presentation :

* '''Add Multi Arch annotations''' to your packages. This is essential to cross compile packages automatically. We are still [not able to cross compile](http://wiki.debian.org/DebianBootstrap/TODO) a minimal debian system in debian because we still miss many multi-arch annotations. Experiments show that with these annotations, these packages will cross compile just fine. A lot of [work](https://wiki.linaro.org/Platform/DevPlatform/CrossCompile/UsingMultiArch) has been done in this direction by Wookey.

* Debian should '''consider adding build profiles''' to build dependencies. [Build Profiles](http://lists.mister-muffin.de/pipermail/debian-bootstrap/2012-July/000283.html) are global build dependencies filters to create packages with a different set of functionalities. Build profiles are of the form ``` Build-Depends: foo (>=0.1) [amd64] <!stage1 bootstrap> | bar```.

This week we just reached an [important
milestone](http://lists.mister-muffin.de/pipermail/debian-bootstrap/2012-November/000425.html)
toward a fully automatic bootstrap procedure. Hopefully we are going
to tell you more about this work during  [fosdem
2013](https://fosdem.org/2013/)

My slides are attached.

* [Attachment 0](/files/debminiconf2012.pdf)
