Title: OSS2017 Adoption of academic tools in open source communities: The Debian case Study
Slug: oss2017
Date: 2017-05-24 13:39:30

This is a joint work with Roberto Di Cosmo presented at OSS2017 in Buenos Aires, Argentina.

Abstract
========

  Component repositories play a key role in the open software ecosystem.
  Managing the evolvution of these repositories is a challenging task,
  and maintainers are confronted with a number of complex issues that
  need automatic tools to be adressed properly.
   
  In this paper, we present an overview of 10 years of research in this
  field and the process leading to the adoption of our tools in a FOSS
  community.  We focus on the Debian distribution and in
  particular we look at the issues arising during the distribution
  lifecycle: ensuring buildability of source packages, detecting
  packages that cannot be installed and bootstrapping the distribution
  on a new architecture. We present three tools, {\em distcheck}, {\em
  buildcheck} and {\em botch}, that we believe of general interest for
  other open source component repositories.
   
  The lesson we have learned during this journey may provide useful
  guidance for researchers willing to see their tools broadly adopted by
  the community.

Paper: [oss2017.pdf](files/oss2017.pdf)

Slides: [oss2017-slides.pdf](files/oss2017-slides.pdf)
