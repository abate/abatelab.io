Title: terminator terminal
Slug: terminator-terminal
Tags: xfce4
Date: 2012-04-08 12:02:43 CET+0000

Despite the fact I can't stop talking about xfce4, what I'm have been
missing lately is the functionality of gnome-terminal... I know I
could install gnome terminal and use it, but this will imply a lot of
unwanted dependencies and I prefer not go that way. Chatting on #xfce
in IRC it was suggested to give a try to **terminator**
https://launchpad.net/terminator/. I've used it for a couple of days
now and I feel very comfortable with it. It has a lot of configuration
options, the possibility to have multiple profiles and to group
windows together, it has tabs and all bells and whistles you can ask.

The only thing I didn't like from the default settings is the red
title bar on top... to remove it you need to edit the config file in
*~/.config/terminator/config* and add **show_titlebar = False** . This
is my conf file :

```
[global_config]
  geometry_hinting = False
  dbus = True
  focus = mouse
  borderless = True
[keybindings]
[profiles]
  [[default]]
    scrollbar_position = hidden
    show_titlebar = False
  [[presentation]]
    use_system_font = False
    font = Monospace 13
[layouts]
  [[default]]
    [[[child1]]]
      profile = default
      type = Terminal
      parent = window0
    [[[window0]]]
      type = Window
      parent = ""
[plugins]
```

Something else I'd like to do is to reduce the size and font of the
tab on top of the terminal. Since this is a gtk applications, I should
hack my gtk theme in order to style the tab, but I don't know how to
do it only for one application. If somebody has an answer I'm all
ears.



