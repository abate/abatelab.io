Title: apt-get dist-upgrade
Slug: aptget-distupgrade
Tags: debian
Date: 2009-04-27 07:57:23 CET+0000

During the weekend I upgraded my laptop to sqeeze. I usually track
unstable pretty closely, but in between transition I gave myself a bit
of slack in order to avoid messing up with the gnome transition. The
result is ok, NetworkManager Just Work !!!, the new kernel seems
pretty snappy. I finally get the power status for my network card.

My laptop is a old dell latidute x200. I always had problem with the
graphic card and Xorg. With this upgrade I've always motivated myself
to find a solution. Not surprisingly it was quite easy. I've added
these option to my xorg.conf :
```
Section "Device"
        Identifier "Configured Video Device"
        Driver "intel"
        Option "Tiling" "false"
        Option "FramebufferCompression" "false"
        Option  "XAANoOffscreenPixmaps" "true"
        Option  "AccelMethod" "EXA"
        Option  "ExaNoComposite" "true"
EndSection
```
I'm not entirely sure if I need them all. I've noticed that already my
screen corruptions go away with "tiling" and "framebuffercompression"
set to false. But the life changing options are the accell method (EXA
seems much more stable) and the "ExaNoComposite".

What I've left to figure out is to fix the hibernate function, that is
still not very reliable as it works 8 out of 10 times.

After 1.3Gb of updates, I'm happy I'm again surfing the unstable wave.

