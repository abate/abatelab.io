Title: bootstrap puppet with ganeti
Slug: bootstrap-puppet-ganeti
Tags: debian, ganeti, puppet
Date: 2012-05-22 07:45:07 CET+0000

[Third post about
ganeti](http://mancoosi.org/~abate/add-swap-hook-ganetideboostrapinstance).

Ganet-debootstrap-instance contains a nice set of scripts to create a
debian (or derivatives) image using debootstrap. Images can be
configured and customized by writing simple hooks script to modify
various aspects of the default installation. However writing these
script is not really fun and pushing it too far can lead to long messy
scripts, loosing the overall benefit of automatic configuration.

[Puppet](http://en.wikipedia.org/wiki/Puppet_%28software%29) is my
configuration management tool of choice, but installing puppet on a
new machine requires few magic incantations that the user should
perform manually, or in a semi automatic mode (*autosign=true*) to
make it work. My goal is to install puppet automatically on the newly
created instance so it will run and configure the new instance at the
first boot. From that moment on I'll forget about ganeti and configure
all remaining services of my new VM using puppet.

In order to do so, we need to install puppet (and apt-get
update/upgrade...), create the ssl certificates for the client and
enabling the puppet daemon on the client. We add another hook in
*/etc/ganeti/instance-debootstrap/hooks/* :

```bash
if [ -z "$TARGET" -o ! -d "$TARGET" ]; then
  echo "Missing target directory"
  exit 1
fi

LANG=C
chroot "$TARGET" apt-get -y --force-yes update
chroot "$TARGET" apt-get -y --force-yes upgrade

# install puppet on the client
chroot "$TARGET" apt-get -y --force-yes install puppet

DOMAIN=localnet.org
instance=$INSTANCE_NAME.$DOMAIN

echo "Installing puppet certificates for $instance"
puppetca clean $instance
puppetca -g $instance

mkdir -p $TARGET/etc/puppet
mkdir -p $TARGET/var/lib/puppet/ssl/private_keys/
mkdir -p $TARGET/var/lib/puppet/ssl/certs/

cp /var/lib/puppet/ssl/private_keys/$instance.pem $TARGET/var/lib/puppet/ssl/private_keys/
rm -f $TARGET/var/lib/puppet/ssl/public_keys/$instance.pem

cp /var/lib/puppet/ssl/certs/$instance.pem $TARGET/var/lib/puppet/ssl/certs/
cp /var/lib/puppet/ssl/certs/ca.pem $TARGET/var/lib/puppet/ssl/certs/

chown root. $TARGET/var/lib/puppet/ssl/private_keys/$instance.pem
chmod 0400 $TARGET/var/lib/puppet/ssl/private_keys/$instance.pem

chown root. $TARGET/var/lib/puppet/ssl/certs/$instance.pem
chmod 0640 $TARGET/var/lib/puppet/ssl/certs/$instance.pem

chown root. $TARGET/var/lib/puppet/ssl/certs/ca.pem
chmod 0641 $TARGET/var/lib/puppet/ssl/certs/ca.pem

#echo "server=puppet" >> /etc/puppet/puppet.conf

echo "START=yes" > $TARGET/etc/default/puppet
echo "DAEMON_OPTS=\"\"" >> $TARGET/etc/default/puppet
```

This script uses *puppetca* to create on the puppet (and ganeti)
server the client key, sign it, and then copy it to the target
machine. Notice that we create the certificate for a fqnd name
```$INSTANCE_NAME.$DOMAIN``` or otherwise puppet will complain loudly.
This is not strictly needed, but if you want to do otherwise, you'll
need to fiddle with the puppet configuration a bit more. The procedure
to create a puppet certificate server-side is well documented on the
puppet website, so if you are curious about the details duck-duck-it .

