#!/bin/bash

DIR=$(readlink -f $PWD)
echo $DIR

#pip3 install virtualenv
virtualenv -p /usr/bin/python3 --no-site-packages $DIR

source $DIR/bin/activate

pip3 install -r requirements.txt

if [ ! -d $DIR/plugins ]; then
  git submodule add https://github.com/getpelican/pelican-plugins $DIR/plugins
fi

git submodule init
git submodule update
